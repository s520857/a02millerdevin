var Calculate = {
    launch: function() {
        Calculate.getName();
        Calculate.getHeight();
        Calculate.getWeight();
        Calculate.getBMI();
    },
    getName: function () {
        let answer = prompt("What is your name?","Sam Smith");
        if (answer != null) {
            $("#name").html(answer);
        }
    },
    getHeight: function () {
        let answer = prompt("How tall are you in inches?","70");
        if (answer != null) {
            $("#height").html(answer);
        }
    },
    getWeight: function () {
        let answer = prompt("How much do you weight in pounds?","150");
        if (answer != null) {
            $("#weight").html(answer);
        }
    },
    getBMI: function () {
        let height = parseFloat($('#height').html());
        let weight = parseFloat($('#weight').html());
        name = document.getElementById("name").textContent;

        heightInM = height * 0.025
        weightInKg = weight * 0.45
        heightInM = heightInM * heightInM
        bmi = weightInKg / heightInM
        alert("At the height of " + height + " inches and weight of " + weight + " pounds, " + name + " has a BMI of " + bmi);
        $("#name").html("Name: " + name);
        $("#height").html("Height in inches: " + height);
        $("#weight").html("Weight in pounds: " + weight);
        $("#bmi").html("BMI(Body Mass Index): " + bmi);

        if (bmi < 18.5) {
            $("#health").html("You BMI is under 18.5 meaning you are considered underweight.");
        } else if (bmi >= 18.5 && bmi < 25.0) {
            $("#health").html("Your BMI is between 18.5 and 25 which is considered very healthy.");
        } else if (bmi >= 25.0 && bmi < 30.0) {
            $("#health").html("Your BMI is between 25 and 30 which is considered overweight.");
        } else {
            $("#health").html("Your BMI is greater than 30 which is considered obese.");
        }
    }
 }